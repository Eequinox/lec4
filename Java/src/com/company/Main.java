package com.company;
import java.util.Random;
public class Main {

    public static void main(String[] args) {

        int n = 9, m = 6;
        float[][] A = new float[n][m];
        float[][] B = new float[n][m];
        float[][] C = new float[n][m];
        float Sum = 0;
        float MaxElement;

        Random rand = new Random();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {

                A[i][j] = (rand.nextFloat() % 100);

            }
        }
        System.out.println("Matrix A:");
        System.out.println();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.printf("%10f", A[i][j]);

            }
            System.out.println();
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {

                B[i][j] = rand.nextFloat() ;

            }
        }
        System.out.println("Matrix B:");
        System.out.println();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.printf("%10f", B[i][j]);

            }
            System.out.println();
        }


        for(int i = 0; i < n; i++) {
            for(int j = 0; j < m; j++) {
                C[i][j] = A[i][j] + B[i][j];

            }

        }
        System.out.println("Matrix C:");
        System.out.println();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {

                System.out.printf("%10f", C[i][j]);

            }
            System.out.println();
        }

        for (int j = 0; j < m ; j++) {
            MaxElement = C[0][j];
            for (int i = 1; i < n; i++) {

                if( C[i][j] > MaxElement)
                    MaxElement = C[i][j];

            }
            Sum+= MaxElement;
            System.out.println("\nMax value in [" + (j+1)+"] row is : " + MaxElement);


        }
        System.out.println("\nResult is :" + Sum);
    }


}
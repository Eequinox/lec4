import java.util.Scanner;
public class program {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		double result = 0, C = 0;
		int n , m ;
		try {
            System.out.print("Input value n: ");
            if(!in.hasNextInt()){
                throw new Exception("Incorrect input!");
            }
            n = in.nextInt();
            if (n < 0)
                throw new Exception("Error! Value of this variable can't be negative");


            System.out.print("Input value m: ");
            if(!in.hasNextInt()){
                throw new Exception("Incorrect input!");
            }
            m = in.nextInt();
            if (m < 0)
                throw new Exception("Error! Value of this variable can't be negative");

            for (double i = 0; i <= n; i++) {
                for (double j = 0; j <= m; j++) {

                    if ((i - C) != 0 && j != 0)
                    	result += (i % j) / (double) i;

                }
            }
            System.out.print("Value result = " + result);
            
		}
		catch(Exception neException){
            System.out.print(neException.getMessage());
            System.exit(1);
	}
	
	}
}
	

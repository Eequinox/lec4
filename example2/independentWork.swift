//
//  independentWork.swift
//  example2
//
//  Created by EQUINOX on 05.10.2017.
//  Copyright © 2017 EQUINOX. All rights reserved.
//

import UIKit

class independentWork: NSObject {
    static func hello(){
        print("\nFirst Assignment:")
        print("Hello World\n")
    }
    
    static func helloQuantityTime(number: Int){
        print("Second Assignment:")
        for _ in 0..<number {
            print("Hello World")
        }
        
    }
    static func weak(number: Int){
        print("\nThird Assignment:")
        switch number {
        case 1:
            print("Mondey")
        case 2:
            print("Tuesday")
        case 3:
            print("Wednesday")
        case 4:
            print("Thursday")
        case 5:
            print("Friday")
        case 6:
            print("Saturday")
        case 7:
            print("Sunday")
        default:
            print("Error")
        }
    }
    static func unicWeak(number:Int){
        print("\nFourth Assignment:")
        switch number%7 {
        case 1:
            print("Monday")
        case 2:
            print("Tuesday")
        case 3:
            print("Wednesday")
        case 4:
            print("Thursday")
        case 5:
            print("Friday")
        case 6:
            print("Saturday")
        default:
            print("Sunday")
        }
    }
       static func fibonachi(number: Int)  {
            print("\nFifth Assignment:")
            var f1=1, f2=1, fib=0
            for i in 3...number {
                fib = f1 + f2
                print("Fibonacci: \(i) = \(fib)")
                f1 = f2
                f2 = fib
        }
    
    }
    static func unitСonverter(number: Int){
        print("\nSixth Assignment:")
        let oneInch = 2.54
        let centimeters = Double (number) * oneInch
        print(number,"inch = ",centimeters,"sm")
    }
    
    static func singSong(number: Int) {
        print("\nSeventh Assignment:")
        for i in stride(from: number, to: 0 , by: -1){
            print(i, "bottles of beer on the wall. \(i) bottles of beer. You take one down, pass it around.")
        }
    }
    static func mathDoing(number: Int){
        print("\nEight Assignment:")
        for i in 1..<101{
            if i % number == 0 {
                print(i)
            }
        }
    }
}



//
//  ViewController.swift
//  example2
//
//  Created by EQUINOX on 05.10.2017.
//  Copyright © 2017 EQUINOX. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var InputTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let number = Int(InputTextField.text!)!
        independentWork.hello()
        independentWork.helloQuantityTime(number:number)
        independentWork.weak(number:number)
        independentWork.unicWeak(number:number)
        independentWork.fibonachi(number:number)
        independentWork.unitСonverter(number:number)
        independentWork.singSong(number:number)
        independentWork.mathDoing(number:number)
        
    }

}



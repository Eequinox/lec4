-- 1.
INSERT INTO employees ("EmployeeID", "LastName", "FirstName", "Title", "City","Country")
VALUES (10, 'Sytnyk', 'Michael', 'Intern', 'Kiev','OAE');

-- 2.
UPDATE employees SET "Title" = 'Director'
WHERE "LastName" = 'Sytnyk' AND "FirstName" = 'Michael';

-- 3.
CREATE TABLE OrdersArchive AS (SELECT * FROM orders) WITH DATA;

-- 4.
TRUNCATE TABLE OrdersArchive;

-- 5.
INSERT INTO  OrdersArchive SELECT * FROM orders WHERE orders."OrderID" NOT IN
(SELECT orders_archive."OrderID" FROM orders_archive);

-- 6.
DELETE FROM orders_archive WHERE "ShipCity" = 'Berlin';

-- 7.
INSERT INTO Products (ProductName, UnitPrice)
VALUES ('Michael', 100);
INSERT INTO Products (ProductName, UnitPrice)
VALUES ('IP-63', 50);

-- 8.
UPDATE products
SET "Discontinued" = 1
WHERE products."ProductID" NOT IN (SELECT order_details."ProductID"
                                   FROM order_details);

-- 9. Видалити таблицю OrdersArchive.
DROP TABLE IF EXISTS orders_archive;

-- 10. Видатили базу Northwind.
DROP DATABASE IF EXISTS northwind;

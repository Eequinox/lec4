-- 1.
CREATE DATABASE Sytnik;

-- 2.
DROP TABLE IF EXISTS student;
DROP VIEW IF EXISTS student_with_age;
DROP VIEW IF EXISTS v_male_student;
DROP SEQUENCE IF EXISTS student_id_seq;
DROP VIEW IF EXISTS v_female_student;

CREATE TABLE IF NOT EXISTS student (
student_id INTEGER NOT NULL, first_name TEXT NOT NULL,last_name TEXT NOT NULL, sex CHARACTER NOT NULL
);

-- 3.
ALTER TABLE student ADD PRIMARY KEY (student_id);

-- 4.
CREATE SEQUENCE IF NOT EXISTS student_id_seq
INCREMENT BY 1
START WITH 1;

ALTER TABLE student
ALTER COLUMN student_id
SET DEFAULT nextval('student_id_seq');

-- 5.
ALTER TABLE student
ADD COLUMN birth_date DATE;

-- 6.
ALTER TABLE Students
ADD CurrentAge INTEGER DEFAULT (DATEDIFF(NOW(), BirthDate)/365);

-- 7.
ALTER TABLE student ADD CONSTRAINT sex_check CHECK (sex = 'f' OR sex = 'm');

-- 8.
INSERT INTO student(first_name, last_name, sex, birth_date) VALUES
('Misha', 'Sytnik', 'm', '1998-09-15'),
('Vladik', 'Popov', 'm', '2005-04-25'),
('Vladik', 'Kekus', 'm', NULL);

-- 9.
CREATE VIEW v_male_student AS
SELECT * FROM student
WHERE sex = 'm';

CREATE VIEW v_female_student AS
SELECT * FROM student
WHERE sex = 'f';

-- 10.
DROP VIEW IF EXISTS v_male_student;
DROP VIEW IF EXISTS v_female_student;
DROP VIEW IF EXISTS v_student_with_age;

ALTER TABLE student
ALTER COLUMN student_id
SET DATA TYPE SMALLINT;



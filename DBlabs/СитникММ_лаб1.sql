/*
1.
*/
SELECT 'Sytnik'      AS	"Last Name",
       'Misha'      AS	"Name",
       'Mikhailovich' AS	"Surname";

/*
2.
*/
SELECT * FROM Products;

/*
3.
*/
SELECT * FROM Products WHERE "Discontinued" LIKE 1;

/*
4.
*/
SELECT DISTINCT "City" FROM Customers;

/*
5.
*/
SELECT "CompanyName" FROM suppliers ORDER BY "CompanyName" DESC;
/*
6.
*/
SELECT "OrderID" AS "1",
       "ProductID" AS "2",
       "UnitPrice" AS "3",
       "Quantity" AS "4",
       "Discount" AS "5"
FROM order_details;
/*
7.
*/
SELECT "ContactName"
  FROM customers
 WHERE "ContactName" LIKE 'S%'
    OR "ContactName" LIKE 'M%'
    OR "ContactName" LIKE 'M%';

/*
8.
*/
SELECT * FROM orders WHERE "ShipAddress" LIKE '% %';
/*
9.
*/
SELECT "ProductName" FROM products WHERE "ProductName" LIKE '\%%a' OR "ProductName" LIKE '\_%a';
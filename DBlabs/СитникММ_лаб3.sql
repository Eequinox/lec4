-- First Exercise:
-- 1.
SELECT last_name, first_name, patronymic FROM (SELECT
        'Michael' AS first_name,
        'Sytnik' AS last_name,
        'Michaelovich' AS patronymic) AS full_name;



-- 2.
SELECT CASE WHEN 22 <= ALL (SELECT * FROM generate_series(1, 24)) THEN ';-)' ELSE ':-D' END;


-- 3.
SELECT * FROM (VALUES
        ('Ludmila', 'Korolieva'),
        ('Vera', 'Popova'),
        ('Viktoriya', 'Bondar')) AS girls(first_name, last_name)
WHERE first_name NOT IN (
    'Marina',
    'Natasha',
    'Anna',
    'Vlada',
    'Ksenia',
    'Julia',
    'Anastasia');


-- 4.
SELECT CASE
        WHEN number = 0 THEN 'zero'
        WHEN number = 1 THEN 'one'
        WHEN number = 2 THEN 'two'
        WHEN number = 3 THEN 'three'
        WHEN number = 4 THEN 'four'
        WHEN number = 5 THEN 'five'
        WHEN number = 6 THEN 'six'
        WHEN number = 7 THEN 'seven'
        WHEN number = 8 THEN 'eight'
        WHEN number = 9 THEN 'nine'
        ELSE to_char(number, 'FM9999MI') END FROM (VALUES (1), (7), (42)) as numbers(number);


-- 5.
SELECT * FROM table_a, table_b;

-- Second Exercise
-- 1.
SELECT "OrderID", "ShipVia", CASE
        WHEN "ShipVia" = 1 THEN 'Sytnik'
        WHEN "ShipVia" = 2 THEN 'Michael'
        WHEN "ShipVia" = 3 THEN 'Michaelovich'
        ELSE NULL END FROM orders;


-- 2.
SELECT customers."Country" FROM customers
UNION
SELECT employees."Country" FROM employees
UNION
SELECT orders."ShipCountry" FROM orders
ORDER BY 1;


-- 3.
SELECT "FirstName", "LastName", count("OrderID") as orders FROM employees INNER  JOIN orders ON employees."EmployeeID" = orders."EmployeeID"
WHERE extract(YEAR FROM orders."OrderDate") = 1998 AND extract(QUARTER FROM orders."OrderDate") = 1 GROUP BY "FirstName", "LastName";


-- 4.
WITH max_discount AS (SELECT "Discount" FROM order_details ORDER BY "Discount" DESC LIMIT 1)
SELECT DISTINCT orders."OrderID"
FROM orders INNER JOIN order_details ON orders."OrderID" = order_details."OrderID" INNER JOIN products ON order_details."ProductID" = products."ProductID"
WHERE products."UnitsInStock" > 100 AND order_details."Discount" NOT IN (SELECT * FROM max_discount);

-- 5.
SELECT "ProductName" FROM products
EXCEPT
(SELECT "ProductName" FROM
        products AS p
        INNER JOIN order_details AS od ON p."ProductID" = od."ProductID"
        INNER JOIN orders AS o ON od."OrderID" = o."OrderID"
        INNER JOIN employeeterritories
        AS et ON et."EmployeeID" = o."EmployeeID"
        INNER JOIN territories AS t ON t."TerritoryID" = et."TerritoryID"
        INNER JOIN region AS r ON r."RegionID" = t."RegionID"
    WHERE r."RegionDescription" = 'Southern'
);
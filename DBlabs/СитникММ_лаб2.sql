-- 1. First Exercise:

-- Postgresql
SELECT count(*) AS row_count FROM large_table;

-- Ms sql.
SELECT COUNT_BIG(*) FROM db.schema.table;

-- 2.
SELECT char_length('Sytnik') AS last_name_length;

-- 3.
SELECT replace('Michael Michaelovich Sytnik', ' ', '_') AS full_name;

-- 4.
SELECT last_name, first_name, lower(substring(first_name from 1 for 2) || substring(last_name from 1 for 4) || '@' || last_name || '.com')
    AS email FROM (SELECT 'Sytnik' AS last_name, 'Misha' AS first_name) AS full_name;

-- 5.
SELECT to_char(TIMESTAMP '1998-09-15', 'Day') AS day_of_week;

-- 1. Second Exercise:
SELECT * FROM products LEFT JOIN categories
    ON products."CategoryID" = categories."CategoryID"
    LEFT JOIN suppliers ON products."SupplierID" = suppliers."SupplierID";

-- 2.
SELECT * FROM orders WHERE extract(year from orders."OrderDate") = 1998 AND
                           extract(month from orders."OrderDate") = 4 AND orders."ShippedDate" IS NULL;

-- 3.
SELECT DISTINCT employees."EmployeeID", "LastName",  "FirstName"
FROM employees JOIN employeeterritories ON employees."EmployeeID" = employeeterritories."EmployeeID" JOIN territories
ON employeeterritories."TerritoryID" = territories."TerritoryID" JOIN region ON territories."RegionID" = region."RegionID"
WHERE "RegionDescription" = 'Northern';
-- 4.

 SELECT SUM("UnitPrice" * "Quantity" * (1 - "Discount")) AS "TotalValue"
   FROM order_details
  INNER JOIN orders
     ON orders."OrderID" = order_details."OrderID"
  WHERE EXTRACT(DAY FROM "OrderDate")::INTEGER % 2 = 1;

-- 5.
SELECT "ShipAddress", sum("UnitPrice" * "Quantity" * (1 - "Discount")) AS order_price FROM orders
JOIN order_details ON orders."OrderID" = order_details."OrderID" GROUP BY orders."OrderID"
ORDER BY order_price DESC LIMIT 1;